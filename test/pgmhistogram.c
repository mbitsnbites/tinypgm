#include <stdio.h>
#include <stdlib.h>
#include <tinypgm.h>

int main(int argc, char** argv) {
  tpgm_info_t info;
  char* file_name;
  unsigned char* data;
  unsigned hist[256];
  int i, x, y, pix;

  /* Check input arguments. */
  if (argc < 2) {
    printf("Usage: %s image\n", argv[0]);
    printf("  image - The file to display\n");
    return 1;
  }
  file_name = argv[1];

  /* Get image information. */
  if (!tpgm_load_info(file_name, &info)) {
    fprintf(stderr, "Failed to load image.\n");
    return 1;
  }
  printf("Image dimensions: %dx%d (max value: %d), total pixels: %d\n",
         info.width, info.height, info.max_value, info.width * info.height);

  /* Allocate memory for the image data. */
  data = (unsigned char*)malloc(info.data_size);
  if (!data) {
    fprintf(stderr, "Failed to allocate memory for the image data.\n");
    return 1;
  }

  /* Load the image data. */
  if (!tpgm_load_data(file_name, NULL, data, info.data_size)) {
    fprintf(stderr, "Failed to load the image data.\n");
    free(data);
    return 1;
  }

  /* Calculate the histogram. */
  for (i = 0; i < 256; ++i) {
    hist[i] = 0U;
  }
  for (y = 0; y < info.height; ++y) {
    for (x = 0; x < info.width; ++x) {
      pix = data[y * info.width + x];
      hist[pix]++;
    }
  }

  /* Print the histogram. */
  for (i = 0; i < 256; ++i) {
    printf("%d", hist[i]);
    if (i < 255) {
      printf(",");
    } else {
      printf("\n");
    }
  }

  /* Free the image data. */
  free(data);

  return 0;
}
